let nodes = [];
let links = [];
let nodeCount = 0;

const width = 10000;
const height = 10000;

const nodeRadius = 10;

const graphScale = 50000;

// Translate coordinate system to make (0,0) at the center of SVG
function translate(x, y) {
  return {
    x: (x - 14.318208839704235) * graphScale + width / 2, // Subtract 14 from x
    y: height / 2 - (y - 120.9682734199138) * graphScale, // Subtract 120 from y
  };
}

function inverseTranslate(x, y) {
  return {
    x: (x - width / 2) / graphScale + 14.318208839704235,
    y: 120.9682734199138 - (y - height / 2) / graphScale,
  };
}

let nodeCounter = 0;
let nodeName = numberToLetters(nodeCounter);

function numberToLetters(number) {
  const alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let name = "";

  while (number >= 0) {
    const mod = number % 26;
    name = alphabets[mod] + name;
    number = Math.floor(number / 26) - 1;
  }

  return name;
}
function getNodeByName(name) {
  return nodes.find((node) => node.id === name);
}

// D3.js setup
const svg = d3
  .select("#graph")
  .append("svg")
  .attr("width", width)
  .attr("height", height);

// Define arrowhead marker
svg
  .append("defs")
  .append("marker")
  .attr("id", "arrowhead")
  .attr("viewBox", "-0 -5 10 10")
  .attr("refX", 8) // Position of the arrowhead (adjust as needed)
  .attr("refY", 0)
  .attr("orient", "auto")
  .attr("markerWidth", 5)
  .attr("markerHeight", 5)
  .attr("xoverflow", "visible")
  .append("svg:path")
  .attr("d", "M 0,-5 L 10 ,0 L 0,5")
  .attr("fill", "#000000")
  .style("stroke", "none");

const g = svg.append("g");

// Grid lines
const xScale = d3
  .scaleLinear()
  .domain([-width / 2 / graphScale, width / 2 / graphScale])
  .range([0, width]);
const yScale = d3
  .scaleLinear()
  .domain([-height / 2 / graphScale, height / 2 / graphScale])
  .range([height, 0]);

const xAxis = d3
  .axisBottom(xScale)
  .ticks(((width + 2) / (height + 2)) * 10)
  .tickSize(height)
  .tickPadding(8 - height);
const yAxis = d3
  .axisRight(yScale)
  .ticks(10)
  .tickSize(width)
  .tickPadding(8 - width);

g.append("g").attr("class", "grid-line").call(xAxis);
g.append("g").attr("class", "grid-line").call(yAxis);

// Zoom and drag
svg.call(
  d3.zoom().on("zoom", function (event) {
    g.attr("transform", event.transform);
  })
);

const zoom = d3.zoom().on("zoom", function (event) {
  g.attr("transform", event.transform);
});

svg.call(zoom);

// Run Dijkstra's Algorithm
document
  .getElementById("runDijkstraBtn")
  .addEventListener("click", function () {
    const source = prompt("Enter source node id for Dijkstra:").toLowerCase();
    const destination = prompt(
      "Enter destination node id for Dijkstra:"
    ).toLowerCase();

    if (!getNodeByName(source) || !getNodeByName(destination)) {
      alert("Invalid node(s)!");
      return;
    }

    // Run Dijkstra's algorithm
    const { distances, previous } = dijkstra(nodes, links, source);

    const path = [];
    for (let at = destination; at != null; at = previous[at]) {
      path.push(at);
    }
    path.reverse();

    if (path[0] !== source) {
      alert("No path found!");
      return;
    }

    // Update the graph colors
    nodes.forEach((node) => {
      if (path.includes(node.id)) {
        node.color = "green";
      } else {
        node.color = "#999";
      }
    });

    links.forEach((link) => {
      if (path.includes(link.source) && path.includes(link.target)) {
        link.color = "green";
      } else {
        link.color = "#999";
      }
    });
    console.log(`path = ${path}`);

    // After running the algorithm, update the graph to reflect changes
    updateGraph();
  });

function updateGraph() {
  const nodeElements = g.selectAll(".node").data(nodes, (d) => d.id); // Change here: use 'g' instead of 'svg'
  nodeElements
    .enter()
    .append("circle")
    .attr("class", "node")
    .attr("id", (d) => d.id)
    .attr("r", nodeRadius)
    .attr("cx", (d) => d.x)
    .attr("cy", (d) => d.y)
    .on("click", function (event, d) {
      // Adding click event listener here
      console.log(d.id);
    })
    .merge(nodeElements)
    .attr("fill", (d) => d.color || "#999");

  nodeElements.exit().remove();

  const linkElements = g
    .selectAll(".link")
    .data(links, (d) => `${d.source}-${d.target}`);

  linkElements
    .enter()
    .append("path")
    .attr("class", "link")
    .attr("d", function (d) {
      const sourceNode = getNodeByName(d.source);
      const targetNode = getNodeByName(d.target);
      return `M${sourceNode.x},${sourceNode.y}L${targetNode.x},${targetNode.y}`;
    })
    .attr("fill", "none")
    .attr("stroke", (d) => d.color || "#999")
    .attr("marker-end", "url(#arrowhead)")
    .merge(linkElements)
    .attr("d", function (d) {
      const sourceNode = getNodeByName(d.source);
      const targetNode = getNodeByName(d.target);
      return `M${sourceNode.x},${sourceNode.y}L${targetNode.x},${targetNode.y}`;
    })
    .attr("stroke", (d) => d.color || "#999");

  linkElements.exit().remove();
}

function dijkstra(nodes, links, startNode) {
  console.log("running dijkstra");
  const unvisited = nodes.map((n) => n.id);
  const distances = {};
  const previous = {};

  nodes.forEach((node) => {
    distances[node.id] = Infinity; // initialize all distances to Infinity
    previous[node.id] = null; // no previous nodes at the beginning
  });

  distances[startNode] = 0; // distance to the start node is 0

  while (unvisited.length > 0) {
    // find the node with the smallest known distance
    let currentNode = unvisited.reduce((nearest, node) => {
      return distances[node] < distances[nearest] ? node : nearest;
    });

    // remove current node from unvisited set
    unvisited.splice(unvisited.indexOf(currentNode), 1);

    // find neighboring nodes
    let neighbors = links
      .filter((l) => l.source === currentNode)
      .map((l) => l.target);

    for (let neighbor of neighbors) {
      let link = links.find(
        (l) =>
          (l.source === currentNode && l.target === neighbor) ||
          (l.target === currentNode && l.source === neighbor)
      );
      let alt = distances[currentNode] + parseFloat(link.weight);
      if (alt < distances[neighbor]) {
        distances[neighbor] = alt;
        previous[neighbor] = currentNode;
      }
    }
  }

  return { distances, previous };
}

function exportGraph() {
  // Create a copy of the nodes with inverse translation
  const translatedNodes = nodes.map((node) => {
    const inverseTranslated = inverseTranslate(node.x, node.y);
    return {
      ...node,
      x: inverseTranslated.x,
      y: inverseTranslated.y,
    };
  });

  // Update the graph with the translated nodes
  const graph = {
    nodes: translatedNodes,
    links: links,
    nodeCount: nodeCount,
    nodeCounter: nodeCounter,
    nodeName: nodeName,
  };

  // Serialize and store the updated graph
  let serializedGraph = JSON.stringify(graph);
  localStorage.setItem("graphData", serializedGraph);
}

// this function imports the graph data saved in localstorage
function importGraph() {
  let serializedGraph = localStorage.getItem("graphData");
  if (serializedGraph) {
    const graph = JSON.parse(serializedGraph);

    // Translate the imported nodes
    const translatedNodes = graph.nodes.map((node) => {
      const translated = translate(node.x, node.y);
      return {
        ...node,
        x: translated.x,
        y: translated.y,
      };
    });

    // Update the graph with the translated nodes and other data
    nodes = translatedNodes;
    links = graph.links;
    nodeCount = graph.nodeCount;
    nodeCounter = graph.nodeCounter;
    nodeName = graph.nodeName;
    updateGraph(); // Re-render the graph after importing
  } else {
    console.warn("No graph data found in local storage.");
  }
}

document.getElementById("exportGraph").addEventListener("click", exportGraph);
document.getElementById("importGraph").addEventListener("click", importGraph);

// Add node
document.getElementById("addNodeBtn").addEventListener("click", function () {
  let x = parseFloat(prompt("Enter x coordinate:"), 10);
  let y = parseFloat(prompt("Enter y coordinate:"), 10);

  if (isNaN(x) || isNaN(y)) {
    alert("Invalid coordinates!");
    return;
  }

  let translated = translate(x, y);
  nodeCounter++;

  // Convert node name to lowercase
  const lowercaseNodeName = nodeName.toLowerCase();

  nodes.push({ id: lowercaseNodeName, x: translated.x, y: translated.y });
  nodeName = numberToLetters(nodeCounter); // Update the nodeName for the next node.
  updateGraph();
});

// Add edge
document.getElementById("addEdgeBtn").addEventListener("click", function () {
  if (nodes.length >= 2) {
    const source = prompt("Enter source node id:").toLowerCase(); // Convert input to lowercase
    const target = prompt("Enter target node id:").toLowerCase(); // Convert input to lowercase

    if (!getNodeByName(source) || !getNodeByName(target)) {
      alert("One or both nodes don't exist!");
      return;
    }

    if (source === target) {
      alert("Source and target should be different.");
      return;
    }

    // Check if the link already exists
    const existingLink = links.find(
      (link) => link.source === source && link.target === target
    );
    if (existingLink) {
      alert("Link already exists between the specified nodes!");
      return;
    }

    const weight = prompt("Enter edge weight:");
    links.push({ source, target, weight });
    updateGraph();
  }
});

let rotationAngle = 20; // Starting rotation in degrees
let flipHorizontal = 1; // No horizontal flip initially
let flipVertical = 1; // No vertical flip initially
svg.call(
  d3.zoom().on("zoom", function (event) {
    g.attr(
      "transform",
      event.transform +
        ` rotate(${rotationAngle}, ${width / 2}, ${height / 2})` +
        ` scale(${flipHorizontal}, ${flipVertical})`
    );
  })
);
// Adjust by 15 degrees or any other value you'd like
g.transition()
  .duration(500)
  .attr("transform", `rotate(${rotationAngle}, ${width / 2}, ${height / 2})`);
let horizontal = true; // True for horizontal flip, false for vertical flip

if (horizontal) {
  flipHorizontal *= -1;
} else {
  flipVertical *= -1;
}

g.transition()
  .duration(1)
  .attr(
    "transform",
    `rotate(${rotationAngle}, ${width / 2}, ${height / 2})` +
      ` scale(${flipHorizontal}, ${flipVertical})`
  );

function getRandomColor() {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
const routeColors = {};
// this function imports the graph data saved in nodes.js and has to go to processing before it can be displayed in the graph
function customGraph() {
  // Initializing an empty hardcoded graph
  const hardcodedGraph = {
    nodes: [],
    links: [],
  };

  for (const routeName in routes) {
    if (routes.hasOwnProperty(routeName)) {
      if (!routeColors[routeName]) {
        routeColors[routeName] = getRandomColor();
      }
      for (let i = 0; i < routes[routeName].length; i++) {
        const nodeId = `${routeName}_${i}`;
        const nodePosition = routes[routeName][i].position;
        const translatedNodePosition = translate(
          nodePosition.x,
          nodePosition.y
        );

        // Add the node
        hardcodedGraph.nodes.push({
          id: nodeId,
          x: translatedNodePosition.x,
          y: translatedNodePosition.y,
          color: routeColors[routeName],
        });

        if (i !== 0) {
          const previousNodeId = `${routeName}_${
            hardcodedGraph.nodeCounter - 1
          }`;
          hardcodedGraph.links.push({
            source: previousNodeId,
            target: nodeId,
            weight: 1,
          });
        }

        hardcodedGraph.nodeCounter++;
      }
      hardcodedGraph.nodeCount = hardcodedGraph.nodes.length;
    }
  }
  nodes = hardcodedGraph.nodes;
  links = hardcodedGraph.links;

  console.log(nodes);
  updateGraph();
}

// this function imports the graph data saved in nodes2.json which is already formatted and ready to be displayed
function customGraph2() {
  fetch("nodes2.json") // Fetch the JSON file
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json(); // Parse the JSON response
    })
    .then((graph) => {
      // Translate the imported nodes
      const translatedNodes = graph.nodes.map((node) => {
        const translated = translate(node.x, node.y);
        return {
          ...node,
          x: translated.x,
          y: translated.y,
        };
      });

      // Update the graph with the translated nodes and other data
      nodes = translatedNodes;
      links = graph.links;
      updateGraph();
    })
    .catch((error) => {
      console.error("Error fetching or parsing JSON:", error);
    });
}
