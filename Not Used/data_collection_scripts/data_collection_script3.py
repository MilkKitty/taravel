import json

# List of input JSON files to merge
# Replace with your file names
input_files = ["../master_nodelist.json", "../temporary_nodelist.json"]

# Initialize empty lists for nodes and links
merged_nodes = []
merged_links = []

# Iterate through the input files and merge their data
for input_file in input_files:
    with open(input_file, 'r') as file:
        data = json.load(file)

    # Append nodes and links from each file to the merged lists
    merged_nodes.extend(data["nodes"])
    merged_links.extend(data["links"])

# Create the final merged JSON structure
merged_data = {
    "nodes": merged_nodes,
    "links": merged_links
}

# Write the merged JSON to a new file
with open('../master_nodelist.json', 'w') as output_file:
    json.dump(merged_data, output_file, indent=4)

print("Merging completed. Merged output saved to 'master_nodelist.json'.")
