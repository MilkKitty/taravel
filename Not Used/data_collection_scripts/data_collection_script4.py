import json
import math


def haversine_distance(node1, node2):
    R = 6371.0710
    lat1, lon1 = node1["x"], node1["y"]
    lat2, lon2 = node2["x"], node2["y"]
    rlat1 = lat1 * (math.pi / 180)
    rlat2 = lat2 * (math.pi / 180)
    difflat = rlat2 - rlat1
    difflon = (lon2 - lon1) * (math.pi / 180)
    d = 2 * R * math.asin(math.sqrt(math.sin(difflat / 2) * math.sin(difflat / 2) +
                          math.cos(rlat1) * math.cos(rlat2) * math.sin(difflon / 2) * math.sin(difflon / 2)))
    return d


def extract_route_from_id(node_id):
    return "_".join(node_id.split("_")[:-1])


with open("../master_nodelist.json", "r") as file:
    data = json.load(file)

nodes = data["nodes"]
links = data["links"]

node_dict = {node["id"]: node for node in nodes}

for link in links:
    source_node = node_dict[link["source"]]
    target_node = node_dict[link["target"]]
    link["weight"] = haversine_distance(source_node, target_node)

virtual_links = []

for i in range(len(nodes)):
    for j in range(i + 1, len(nodes)):
        weight = 4*haversine_distance(nodes[i], nodes[j])
        if not (extract_route_from_id(nodes[i]["id"]) == extract_route_from_id(nodes[j]["id"]) or (1.5 * weight > 1) or (weight < 0.64)):
            virtual_links.append(
                {"source": nodes[i]["id"], "target": nodes[j]["id"], "weight": weight, "type": "walk"})
            virtual_links.append(
                {"source": nodes[j]["id"], "target": nodes[i]["id"], "weight": weight, "type": "walk"})

links.extend(virtual_links)

with open("../master_nodelist.json", "w") as output_file:
    json.dump(data, output_file, indent=4)

print("Weight update completed. Updated output saved to 'master_nodelist.json'.")
